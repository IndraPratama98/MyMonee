# MyMonee
[![GitLab license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/IndraPratama98/MyMonee)

# Description

This This application is used for to do transaction and list your dreams so you can save your money.

## Profile and Home
![](Images/Screen%20Shot%202021-06-01%20at%2013.15.33.png)
## Dreams
![](Images/Screen%20Shot%202021-06-01%20at%2013.16.46.png)


# Table of Contents 

* [Installation](#installation)

* [Usage](#usage)

* [License](#license)

* [Contributing](#contributing)

* [Tests](#tests)

* [Questions](#questions)

## Installation
The following necessary dependencies must be installed to run the application properly: -



## Usage
### Instructions
- ​This application is used for to do transaction and list your dreams so you can save your money.
- You need to download this file, unzip it and go to the App folder. 
- Open the terminal and install the dependencies using
```
pod install
```
- Open MyMonee.xcodeproj
- Set your simulator and follow the instruction in this Readme Description before
- Congratulations now you can access all the Features


## License

This project is license under the MIT license.

## Contributing

​Contributors: Indra Anugrah Pratama

## Questions

If you have any questions about the repo, open an issue or contact IndraPratama98 directly indra.pratama3224@gmail.com

