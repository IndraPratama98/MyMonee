//
//  NotFoundView.swift
//  MyMonee
//
//  Created by MacBook on 08/05/21.
//

import UIKit

class NotFoundView: UIView {

    
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var mainLabel: UILabel!
    
    //    Constructor untuk pembuatan programtically
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed("NotFoundView",  owner: self, options: nil)
        addSubview(mainView)
        mainView.frame = self.bounds
        mainView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    @IBAction func handlePan(_ gesture: UIPanGestureRecognizer){
        let translation = gesture.translation(in: mainView)
        
        guard let gestureView = gesture.view else {
            return
        }
        
        gestureView.center = CGPoint(
            x: gestureView.center.x + translation.x,
            y: gestureView.center.y + translation.y
        )
        
        gesture.setTranslation(.zero, in: mainView)
        
        guard gesture.state == .ended else {
            return
        }
        
        let velocity = gesture.velocity(in: mainView)
        let magnitude = sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y))
        let slideMultiplier = magnitude / 100
        
        let sliderFactor = 0.1 * slideMultiplier
        
        var finalPoint = CGPoint(
            x: gestureView.center.x + (velocity.x * sliderFactor),
            y: gestureView.center.y + (velocity.y * sliderFactor)
        )
        
        finalPoint.x = min(max(finalPoint.x, 0), mainView.safeAreaLayoutGuide.layoutFrame.width)
        finalPoint.y = min(max(finalPoint.y, 0), mainView.safeAreaLayoutGuide.layoutFrame.height)
        
        UIView.animate(
            withDuration: Double(sliderFactor * 2),
            delay: 0,
            options: .curveEaseOut,
            animations: {
                gestureView.center = finalPoint
            }
        )
        
    }
}
