//
//  CustomTableViewCell.swift
//  MyMonee
//
//  Created by MacBook on 09/05/21.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    
    @IBOutlet var mainLabel: UILabel!
    @IBOutlet var mainLabelPrice: UILabel!
    @IBOutlet var myProgressBar: UIProgressView!

    @IBOutlet weak var deleteButton: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
      
    }
    
    
    @IBAction func maingesture(_ sender: UITapGestureRecognizer){
        showAlert()
    }
    
    @IBAction func didTapButton(_ sender: Any){
         showAlert()
     }
     
     func showAlert() {
         let alert = UIAlertController(title: "Menghapus Impian", message: "Apakah yakin anda ingin menghapus ?", preferredStyle: .alert)
         alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
             print("tappped Dismiss")
         }))
         alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { action in
             print("tappped Delete")
         }))
        window!.rootViewController?.present(alert, animated: true, completion: nil)
     }
    
    
}
