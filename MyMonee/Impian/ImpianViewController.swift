//
//  ViewControllerDemoViewController.swift
//  MyMonee
//
//  Created by MacBook on 09/05/21.
//

import UIKit

class ImpianViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var notFoundView: NotFoundView!
    
    
    @IBOutlet var tableView: UITableView!
    
    let myData = ["Membeli Airpods",
                  "Membeli Sepatu Adidas",
                  "Membeli Mobil"]
    
    let myData1 = ["IDR 999.000 / IDR 1.500.000",
                   "IDR 999.000 / IDR 500.000",
                   "IDR 999.000 / IDR 200.000.000"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let nib = UINib(nibName: String(describing: CustomTableViewCell.self), bundle: nil)
        
        tableView.register(nib, forCellReuseIdentifier: String(describing: CustomTableViewCell.self))
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as! CustomTableViewCell
        
        cell.mainLabel.text = myData[indexPath.row]
        cell.mainLabelPrice.text = myData1[indexPath.row]
        return cell
    }
    
    

}
