//
//  File.swift
//  MyMonee
//
//  Created by MacBook on 11/05/21.
//

import Foundation

struct Pengeluaran {
    var id: Int?
    var pengeluaranName: String?
    var pengeluaranPrice: String?
    var status: Bool = false

    init(id: Int, pengeluaranName: String, pengeluaranPrice: String, status: Bool) {
        self.id = id
        self.pengeluaranName = pengeluaranName
        self.pengeluaranPrice = pengeluaranPrice
        self.status = status
    }
    
    
}
let pengeluaran: [Pengeluaran] = [
    Pengeluaran(id: 1, pengeluaranName: "Bayar Listrik", pengeluaranPrice: "Rp. 200.000", status: false),
    Pengeluaran(id: 2, pengeluaranName: "Bayar Kosan", pengeluaranPrice: "Rp. 300.000", status: true),
    Pengeluaran(id: 3, pengeluaranName: "Bayar Kosan orang lain", pengeluaranPrice: "Rp. 400.000", status: false),
    Pengeluaran(id: 4, pengeluaranName: "Bayar Listrik", pengeluaranPrice: "Rp. 500.000", status: true),
]
